import React, {} from 'react';
import {Text,TouchableOpacity,} from 'react-native';

const LoginButton = props => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: props.color,
        paddingVertical: 14,
        marginTop: 20,
        marginHorizontal: 25,
        borderRadius: 50,
        elevation: 2,
      }}>
      <Text style={{color: '#FFFFFF', textAlign: 'center', fontWeight: 'bold'}}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

export default LoginButton;
