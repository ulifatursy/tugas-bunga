import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Input from '../component/Input';

const Chat = () => {
 
  const [saran, setSaran] = useState('');

  return (
    <View style={styles.box}>
      <Text style={{fontSize: 22, fontWeight: 'bold', color: '#FFFFFF'}}> Ada yang bisa kami bantu?</Text>
      
     
       <Input
        label="Saran"
        placeholder="Ketik pesan"
        value={saran}
        onChangeText={(text) => setSaran(text)}
      />
            <Text style={styles.text}> pesan: {saran}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    padding: 15,
    flex: 1,
    backgroundColor: '#B0C4DE',
  },
  text: {
    color: '#FFFFFF',
    marginTop: 15,
  },
});

export default Chat;