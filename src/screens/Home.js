import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
  StatusBar,
} from 'react-native';


const Home = () => {
  const [dataBunga, setDataBunga] = useState([
    {
      bunga: 'Mawar',
      image: require('../images/mawar.jpg'),
      harga: '10.000/tangkai',
    },
    {
      bunga: 'Anggrek',
      image: require('../images/anggrek.jpg'),
      harga: '50.000/tangkai',
    },
    {
      bunga: 'Baby breath',
      image: require('../images/baby.jpg'),
      harga: '30.000/seikat',
    },
    {
      bunga: 'Lavender',
      image: require('../images/lav.jpg'),
      harga: '20.000/seikat',
    },
    {
      bunga: 'Daisy',
      image: require('../images/daisy.jpg'),
      harga: '8.000/tangkai',
    },
    {
      bunga: 'Daffodil',
      image: require('../images/daffodil.jpg'),
      harga: '6.000/tangkai',
    },
  ]);
  const [datadisarankan, setDatadisarankan] = useState([
    {
      bunga: 'Tulip',
      image: require('../images/tulip.jpg'),
      harga: '10.000/tangkai',
    },
    {
      bunga: 'Krisan',
      image: require('../images/krisan.jpg'),
      harga: '25.000/tangkai',
    },
    {
      bunga: 'Baby breath',
      image: require('../images/baby.jpg'),
      harga: '30.000/seikat',
    },
    {
      bunga: 'Matahari',
      image: require('../images/matahari.jpg'),
      harga: '20.000/tangkai',
    },
    {
      bunga: 'Daisy',
      image: require('../images/daisy.jpg'),
      harga: '8.000/tangkai',
    },
    {
      bunga: 'Lily',
      image: require('../images/lily.jpg'),
      harga: '30.000/tangkai',
    },
  ]);

  const [daftarRekomendasi, setDaftarRekomendasi] = useState([
    {
      bunga: 'KRISAN',
      deskripsi: 'banyak macam-macam warnanya,segar,cantik ',
      image: require('../images/krisan.jpg'),
    },
    {
      bunga: 'MATAHARI',
      deskripsi: 'warna menarik, segar,dan tahan lama ',
      image: require('../images/matahari.jpg'),
    },
    {
      bunga: 'LILY',
      deskripsi: 'Elegan, segar,dan wangi ',
      image: require('../images/lily.jpg'),
    },
    {
      bunga: 'TULIP',
      deskripsi: 'bentuk yang menarik, segar,dan cantik ',
      image: require('../images/tulip.jpg'),
    },
  ]);

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="#B0C4DE" barStyle="light-content" />
      <ScrollView style={{flex: 1}}>
        <View
          style={{
            backgroundColor: '#B0C4DE',
            borderBottomLeftRadius: 30,
            paddingBottom: 10,
            elevation: 5,
          }}>
          <View style={{marginHorizontal: 20, marginTop: 10}}>
            <Text style={{fontSize: 22, fontWeight: 'bold', color: '#FFFFFF'}}>
              Fresh flower
            </Text>
          </View>

          <View style={{marginLeft: 20, marginTop: 20}}>
            <FlatList
              data={daftarRekomendasi}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    backgroundColor: '#ffffff',

                    marginTop: 10,
                    flexDirection: 'row',
                    marginRight: 20,
                    elevation: 3,
                    padding: 10,
                    marginBottom: 10,
                    borderRadius: 5,
                  }}
                  onPress={() => setDaftarRekomendasi()}>
                  <View style={{marginRight: 10, width: 200}}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#212121',
                      }}>
                      {item.bunga}
                    </Text>
                    <Text style={{fontSize: 14}}>{item.deskripsi}</Text>
                  </View>
                  <View>
                    <Image
                      source={item.image}
                      style={{width: 130, height: 150, borderRadius: 5}}
                      resizeMode="contain"
                    />
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>

        <View style={{marginLeft: 20, marginTop: 20}}>
          <View style={{flexDirection: 'row', marginRight: 10}}>
            <Text style={{fontWeight: 'bold', color: '#212121'}}>
              bunga pilihan
            </Text>
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <Text>Lihat Semua</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={datadisarankan}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  width: 150,
                  backgroundColor: '#ffffff',

                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  marginTop: 10,
                }}
                onPress={() => setDatadisarankan()}>
                <Image
                  source={item.image}
                  style={{width: 130, height: 150, borderRadius: 5}}
                  resizeMode="contain"
                />
                <Text style={{fontWeight: 'bold'}}>{item.bunga}</Text>
                <Text style={{fontSize: 14}}>{item.harga}</Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View style={{marginLeft: 20, marginTop: 20, marginBottom: 20}}>
          <View style={{flexDirection: 'row', marginRight: 10}}>
            <Text style={{fontWeight: 'bold', color: '#212121'}}>disarankan</Text>
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <Text>Lihat Semua</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={dataBunga}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  width: 150,
                  backgroundColor: '#ffffff',

                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  marginTop: 10,
                }}
                onPress={() => setDataBunga()}>
                <Image
                  source={item.image}
                  style={{width: 130, height: 150, borderRadius: 5}}
                  resizeMode="contain"
                />
                <Text style={{fontWeight: 'bold'}}>{item.bunga}</Text>
                <Text style={{fontSize: 14}}>{item.harga}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>

      
    </View>
  );
};

export default Home;
