import React, {useState} from 'react';
import {View, Text,Image, ScrollView,} from 'react-native';
import TextInputEmail from '../component/TextInputEmail';
import LoginButton from '../component/LoginButton';

const profile = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
       <ScrollView>
        <View
          style={{
            flex: 1, 
            backgroundColor: '#B0C4DE',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 60,
          }}>
          <Image
            source={require('../images/11.png')}
            style={{width: 200, height: 200}}
          />
          <Text style={{fontSize: 28, fontWeight: 'bold'}}>
            FLO<Text style={{color: '#2396F2'}}>RIST</Text>
          </Text>
          <Text style={{marginTop: 10, fontWeight: 'bold', fontSize: 18}}>
            Login
          </Text>

        </View>
  
        <TextInputEmail
          state={email}
          set={setEmail}
          icon="envelope"
          placeholder="Masukkan email"
          isPassword={false}
        />
        <TextInputEmail
          state={password}
          set={setPassword}
          icon="lock"
          placeholder="masukkan password"
          isPassword={true}
        />
        <LoginButton text="Login" color="#2396F2" />
  
        
      </ScrollView>
    );
};

export default profile;